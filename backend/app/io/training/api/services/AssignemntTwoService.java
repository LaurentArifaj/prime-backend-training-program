package io.training.api.services;

import akka.japi.Pair;
import akka.stream.impl.fusing.Reduce;

import com.google.inject.Inject;
import com.mongodb.connection.Stream;

import io.training.api.models.requests.BinarySearchRequest;
import io.training.api.models.requests.NameValuePair;
import io.training.api.models.responses.ChartData;
import lombok.val;
import play.api.data.Mapping;
import play.Logger;
import play.libs.concurrent.HttpExecutionContext;
import views.html.helper.input;

import javax.inject.Singleton;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.concurrent.CompletionException;
import java.util.stream.Collectors;

/**
 * Created by Agon on 09/08/2020
 */
@Singleton
public class AssignemntTwoService {
    @Inject
    HttpExecutionContext ec;


    /**
     * Function as Line: y = x * 2
     * @param input
     * @return
     */
    public CompletableFuture<List<Integer>> function1 (List<Integer> input) {
        return CompletableFuture.supplyAsync(() -> {
            return input.stream().map(i->i*2).collect(Collectors.toList());
        }, ec.current());
    }


    /**
     * Function as Scatter y = square root of the absolute value of ((x ^ 2) + (x * 4))
     * @param input
     * @return
     */
    public CompletableFuture<List<Double>> function2 (List<Integer> input) {
        return CompletableFuture.supplyAsync(() -> {
            return input.stream().map(i-> Math.sqrt(Math.abs(Math.pow(i, 2) + (i * 4)))).collect(Collectors.toList());
        }, ec.current());
    }

    /**
     * Function y = If 3^2 - x^2 > 0 than square root of (3^2 - x^2). If 3^2 - x^2 < 0 then - square root of absolute value of (3^2 - x^2)
     * @param input
     * @return
     */
    public CompletableFuture<List<Double>> function3 (List<Integer> input) {
        return CompletableFuture.supplyAsync(() -> {
            return input.stream().map(i-> (((Math.pow(3, 2) - Math.pow(i, 2)) > 0) ?
                Math.sqrt(Math.pow(3, 2) - Math.pow(i, 2)) : (-1) * Math.sqrt(Math.abs(Math.pow(3, 2) - Math.pow(i, 2)))))
                .collect(Collectors.toList());
        }, ec.current());
    }

    /**
     * Function as Line: y = sin(x)
     * @param input
     * @return
     */
    public CompletableFuture<List<Double>> function4 (List<Integer> input) {
        return CompletableFuture.supplyAsync(() -> {
            return input.stream().map(i-> Math.sin(i)).collect(Collectors.toList());
        }, ec.current());
    }

    /**
     * Function as Line: y = cos(x)
     * @param input
     * @return
     */
    public CompletableFuture<List<Double>> function5 (List<Integer> input) {
        return CompletableFuture.supplyAsync(() -> {
            return input.stream().map(i-> Math.cos(i)).collect(Collectors.toList());
        }, ec.current());
    }

    /**
     * 2 Line Functions displayed together, one for Sin and one for Cos, check only the series option to include two of them
     * Returns two lists of double the first list for sin, and the second one for cos
     * @param input
     * @return
     */
    public CompletableFuture<List<List<Double>>> function6 (List<Integer> input) {
        return CompletableFuture.supplyAsync(() -> {
            List<List<Double>> result = new ArrayList<>();
            result.add(input.stream().map(i-> Math.sin(i)).collect(Collectors.toList()));
            result.add(input.stream().map(i-> Math.cos(i)).collect(Collectors.toList()));
            return result;
        }, ec.current());
    }

    /**
     * I want to see the top 4 performing words, given the randomCategoryData, the top 4 with the highest random generated value
     * Make sure the values are summed on repeated words (no duplicates)
     * @param input
     * @return
     */
    public CompletableFuture<List<NameValuePair>> function7 (List<NameValuePair> input) {
        return CompletableFuture.supplyAsync(() -> {
            try {
                return input.stream().collect(Collectors.
                    groupingBy(NameValuePair::getName, Collectors.summingInt(NameValuePair::getValue))).entrySet()
                    .stream().map(t-> new NameValuePair(t.getKey(), t.getValue()) ).sorted(Comparator.
                    comparing(NameValuePair::getValue).reversed()).limit(4).collect(Collectors.toList());
            } catch (Exception ex) {
                ex.printStackTrace();
                throw new CompletionException(ex);
            }
        }, ec.current());
    }

    /**
     * Calculate the average within the groups now, and show that here. Check the random Category data on how it generates thosenam
     * @param input
     * @return
     */
    public CompletableFuture<List<ChartData>> function8 (List<NameValuePair> input) {
        return CompletableFuture.supplyAsync(() -> {
            return input.stream().collect(Collectors.
            groupingBy(NameValuePair::getName, Collectors.averagingInt(NameValuePair::getValue))).
            entrySet().stream().map(t-> new ChartData(t.getKey(), t.getValue())).collect(Collectors.toList());
        }, ec.current());
    }

    /**
     * Calculate the values such that they are cumulative, each subsequent is summed with the total so far!
     * @param input
     * @return
     */
    public CompletableFuture<List<NameValuePair>> function9 (List<NameValuePair> input) {
        return CompletableFuture.supplyAsync(() -> {
            AtomicReference<Integer> atomicSum = new AtomicReference<>(0);
            input.forEach(e -> e.setValue(
            atomicSum.accumulateAndGet(e.getValue(), (x, y) -> x + y)));
            return input;
        }, ec.current());
    }

    /**
     * Calculate the values such that they are cumulative, each subsequent is summed with the total so far!
     * @param request
     * @return index - the index of the search
     */
    public CompletableFuture<Integer> binarySearch (BinarySearchRequest request) {
        return CompletableFuture.supplyAsync(() -> {
            Integer low = 0;
            Integer high = request.getValue().size() - 1;
            Integer midpoint;
            System.out.println(request);

            while(low <= high){
            midpoint = low + (high - low)/2;
            if(request.getValue().get(midpoint) == request.getSearch()){
                return midpoint;
            }
            else{
                if(request.getValue().get(midpoint) < request.getSearch()){
                    low =  midpoint + 1;
                }
                else{
                    high =  midpoint - 1;
                }
            }
        }
            return -1;
        }, ec.current());
    }
}
