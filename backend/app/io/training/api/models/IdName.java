package io.training.api.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import scala.collection.LinearSeq;

import java.util.ArrayList;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class IdName {
    private String id;
    private String name;
    private String parentId;
    private ArrayList<IdName> children= new ArrayList<>();

    public void setChildren(ArrayList<IdName> children) {
        this.children = children;
    }

    public ArrayList<IdName> getChildren() {
        return children;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public static ArrayList<IdName> recursivePath(IdName n1, ArrayList<IdName> n2){
        n2.forEach(d -> {
            if (d.getId().equals(n1.getParentId())) {
                d.getChildren().add(n1);
            }
            else {
                recursivePath(n1, d.getChildren());
            }
        });
        return n2;
    }

}
