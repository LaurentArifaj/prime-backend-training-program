package io.training.api.models;

import java.util.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Graph {
    private Map<Vertex, List<Vertex>> adjVertices = new HashMap<>();

    public Map<Vertex, List<Vertex>> getAdjVertices() {
        return adjVertices;
    }

    public void setAdjVertices(Map<Vertex, List<Vertex>> adjVertices) {
        this.adjVertices = adjVertices;
    }

    public void addVertex(String label) {
        adjVertices.putIfAbsent(new Vertex(label), new ArrayList<>());
    }

    public void removeVertex(String label) {
        Vertex v = new Vertex(label);
        adjVertices.values().stream().forEach(e -> e.remove(v));
        adjVertices.remove(new Vertex(label));
    }

    public void addEdge(String label1, String label2) {
        Vertex v1 = new Vertex(label1);
        Vertex v2 = new Vertex(label2);
        adjVertices.get(v1).add(v2);
        adjVertices.get(v2).add(v1);
    }

    public void removeEdge(String label1, String label2) {
        Vertex v1 = new Vertex(label1);
        Vertex v2 = new Vertex(label2);
        List<Vertex> eV1 = adjVertices.get(v1);
        List<Vertex> eV2 = adjVertices.get(v2);
        if (eV1 != null)
            eV1.remove(v2);
        if (eV2 != null)
            eV2.remove(v1);
    }


    public static ArrayList<ArrayList<Integer>> algorithm(Graph graph,int from,int to, ArrayList<ArrayList<Integer>> paths){
        if(graph.getAdjVertices().isEmpty()){
            return paths;
        }

        //To continue :)
        graph.getAdjVertices().entrySet().forEach(t-> {
            if (t.getKey().getLabel().equals(Integer.toString(from))){
                t.getValue().forEach(value -> {
                    paths.get(0).add(Integer.parseInt(value.getLabel()));
                    graph.removeEdge(t.getKey().getLabel(), value.getLabel());
                    algorithm(graph,Integer.parseInt(value.getLabel()), to, paths);
                });
            }

        });

        return paths;
    }

}
