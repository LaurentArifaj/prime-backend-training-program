package io.training.api.models.responses;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by agonlohaj on 20 Aug, 2020
 */
@Data
@AllArgsConstructor
public class ChartData {
	private String name;
	private double value;

	public String getName() {
		return name;
	}

	public ChartData() {
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}
}
