package io.training.api.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.checkerframework.checker.signedness.qual.Constant;

import javax.annotation.processing.Generated;
import javax.validation.Constraint;
import javax.validation.constraints.*;

/**
 * Created by agonlohaj on 28 Aug, 2020
 */
@Data
@EqualsAndHashCode(of = "id")
public class User {

	private String id;

	@NotEmpty(message = "Cannot be empty!")
	private String type;

	@NotEmpty(message = "Cannot be empty!")
	@Pattern(regexp="^[M|F]")
	private String gender;

	@Min(value = 0, message = "Age cannot be lower than 2")
	@Max(value = 90, message = "Age cannot be higher than 90")
	private int age;

	@NotEmpty(message = "Cannot be empty!")
	private String name;

	@NotEmpty(message = "Cannot be empty!")
	private String username;

	@NotEmpty(message = "Cannot be empty!")
	private String lastName;

	@NotEmpty(message = "Cannot be empty!")
	private String avatar;


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public static  void updateUser(User original, User update) {
		
		original.setType(update.getType());
		original.setGender(update.getGender());
		original.setAge(update.getAge());
		original.setName(update.getName());
		original.setUsername(update.getUsername());
		original.setLastName(update.getLastName());
		original.setAvatar(update.getAvatar());

	}
}
