package io.training.api.models.requests;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by agonlohaj on 20 Aug, 2020
 */
@Data
public class BinarySearchRequest {
	private List<Integer> values = new ArrayList<>();
	private Integer search;


public List<Integer> getValue() {
	return values;
}

public Integer getSearch(){
	return search;
}


}