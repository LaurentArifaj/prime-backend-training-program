package io.training.api.models.requests;

import io.training.api.models.BinaryTree;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by agonlohaj on 28 Aug, 2020
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BinaryTreeRequest {
	int value;
    BinaryTree tree;
    
    public int getValue() {
        return this.value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public BinaryTree getTree() {
        return this.tree;
    }

    public void setTree(BinaryTree tree) {
        this.tree = tree;
    }

    public BinaryTreeRequest value(int value) {
        this.value = value;
        return this;
    }

    public BinaryTreeRequest tree(BinaryTree tree) {
        this.tree = tree;
        return this;
    }

}