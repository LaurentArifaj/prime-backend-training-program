package io.training.api.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeInfo.None;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Created by agonlohaj on 28 Aug, 2020
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@NoArgsConstructor
public class BinaryTree {
	Integer value;
	BinaryTree left;
	BinaryTree right;

    public Integer getValue() {
        return this.value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public BinaryTree getLeft() {
        return this.left;
    }

    public void setLeft(BinaryTree left) {
        this.left = left;
    }

    public BinaryTree getRight() {
        return this.right;
    }

    public void setRight(BinaryTree right) {
        this.right = right;
    }

    public BinaryTree value(Integer value) {
        this.value = value;
        return this;
    }

    public BinaryTree left(BinaryTree left) {
        this.left = left;
        return this;
    }

    public BinaryTree right(BinaryTree right) {
        this.right = right;
        return this;
    }

//  Recursive function to find value
    public static BinaryTree findValue(BinaryTree tree, int value) {
            if (tree.getValue() == value ) {
                return tree;
            }
            else if (tree.getRight() == null){
                return new BinaryTree();
            }

            else if (tree.getValue() > value){
                return findValue(tree.getLeft(), value);
            }
            else if (tree.getValue() < value) {
                return findValue(tree.getRight(), value);
            }
           return new BinaryTree();
         }


}