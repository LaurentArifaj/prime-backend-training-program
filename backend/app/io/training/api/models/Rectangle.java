package io.training.api.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Rectangle {
    private String id;
    private int x;
    private int y;
    private int height;
    private int width;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public static boolean points(String id,int x, int y, int width, int height, Point point){
        Point leftDown = new Point(x- width/2, y - height/2);
        Point rightTop = new Point(x + width/2, y + height/2);
        Point rightDown = new Point(x + width/2, y - height/2);
        Point leftTop = new Point(x- width/2, y + height/2);

        if ( (point.getX() >= leftDown.getX() && point.getY() >= leftDown.getY()) &&
             (point.getX() <= rightTop.getX() && point.getY() <= rightTop.getY()) &&
             (point.getX() <= rightDown.getX() && point.getY() >= rightDown.getY()) &&
             (point.getX() >= leftTop.getX() && point.getY() <= leftTop.getY())) {
            return true;
        }
        return false;
    }

}
