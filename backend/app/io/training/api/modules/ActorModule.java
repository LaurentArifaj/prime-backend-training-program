package io.training.api.modules;

import com.google.inject.AbstractModule;
import io.training.api.actors.ConfiguredActor;
import io.training.api.actors.ConfiguredChildActor;
import io.training.api.actors.ConfiguredChildActorProtocol;
import io.training.api.actors.ParentActor;
import play.libs.akka.AkkaGuiceSupport;

public class ActorModule extends AbstractModule implements AkkaGuiceSupport {

    @Override
    protected void configure() {
        bindActor(ConfiguredActor.class, "configured-actor");
        bindActor(ParentActor.class, "parent-actor");
        bindActorFactory(ConfiguredChildActor.class, ConfiguredChildActorProtocol.Factory.class);
    }
}