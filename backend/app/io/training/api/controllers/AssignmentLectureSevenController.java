package io.training.api.controllers;

import com.fasterxml.jackson.databind.JsonNode;
import io.training.api.models.*;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;


import java.util.*;
import java.util.stream.Collectors;

import static io.training.api.models.Graph.algorithm;
import static play.mvc.Results.noContent;
import static play.mvc.Results.ok;

public class AssignmentLectureSevenController {




    public Result richest(Http.Request request){
        CustomModel[] customModels = Json.fromJson(request.body().asJson(), CustomModel[].class);
        ArrayList<CustomModel> data = new ArrayList<>(Arrays.asList(customModels));
        ArrayList<IdValuePair> idValuePairs = new ArrayList<>();
        data = (ArrayList<CustomModel>) data.stream().filter(d -> d.getAge() > 40).collect(Collectors.toList());
        ArrayList<IdValuePair> finalIdValuePairs = idValuePairs;
        data.forEach(d -> finalIdValuePairs.add(new IdValuePair(d.getId(),CustomModel.sumValue(d.getAssets()))));
        idValuePairs = (ArrayList<IdValuePair>) idValuePairs.stream().sorted(Comparator.comparingDouble(IdValuePair::getValue)
                .reversed()).limit(4).collect(Collectors.toList());
        ArrayList<IdValuePair> finalIdValuePairs1 = idValuePairs;
        data = (ArrayList<CustomModel>) data.stream().filter(d -> finalIdValuePairs1.stream().
                anyMatch(i -> i.getId().equals(d.getId()))).collect(Collectors.toList());
        return ok(Json.toJson(data));
    }

    public Result sorted(Http.Request request){
        UserSorted[] userSorted = Json.fromJson(request.body().asJson(), UserSorted[].class);
        ArrayList<UserSorted> sortedUser = new ArrayList<>();
        sortedUser.addAll(Arrays.asList(userSorted));
        sortedUser = (ArrayList<UserSorted>) sortedUser.stream().sorted(
                Comparator.comparing(UserSorted::getFirstName)
                .thenComparingInt(UserSorted::getAge).reversed()).collect(Collectors.toList());
        return ok(Json.toJson(sortedUser));
    }

    public  Result closest(Http.Request request){
        Point point = Json.fromJson(request.body().asJson().get("point"), Point.class);
        Rectangle[] rectangleObject = Json.fromJson(request.body().asJson().get("rectangles"), Rectangle[].class);
        ArrayList<Rectangle> rectangles = new ArrayList<>(Arrays.asList(rectangleObject));
        ArrayList<Rectangle> rectangle2 = new ArrayList<>();

        rectangles.forEach(r -> {
            if(Rectangle.points(r.getId(), r.getX(), r.getY(), r.getWidth(), r.getHeight(), point)){
                 rectangle2.add(r);
            }else {
                noContent(); } });

        ArrayList<IdDoubleValuePair> distanceList = new ArrayList<>();
        if (rectangle2.size() == 0){
            rectangles.forEach(t -> distanceList.add(new IdDoubleValuePair( t.getId(),
                    Math.sqrt(Math.pow(point.getX() - t.getX(), 2) + Math.pow(point.getY()- t.getY(),2 )))));
            Optional<IdDoubleValuePair> rectangle = distanceList.stream().min(Comparator.comparing(IdDoubleValuePair::getDistance));
            Optional<Rectangle> rect = rectangles.stream().filter(r -> r.getId().equals(rectangle.get().getId())).findFirst();
            return ok(Json.toJson(rect.get()));
        }
        return ok(Json.toJson(rectangle2.get(0)));
    }

    public Result hierarchy(Http.Request request){
        IdName[] idNames = Json.fromJson(request.body().asJson(), IdName[].class);
        ArrayList<IdName> names = new ArrayList<>(Arrays.asList(idNames));
        ArrayList<IdName> data = new ArrayList<>();

        names.forEach(t -> {
            if (t.getParentId() == null){
            data.add(t);
        } else {
                IdName.recursivePath(t, data);
            }
        });

        return ok(Json.toJson(data));
    }

    public Result backtracking(Http.Request request){
        int from = request.body().asJson().get("from").asInt();
        int to = request.body().asJson().get("to").asInt();
        IdListPair[] idListPairs = Json.fromJson(request.body().asJson().get("nodes"), IdListPair[].class);
        ArrayList<IdListPair> nodes = new ArrayList<>(Arrays.asList(idListPairs));

        Graph graph = new Graph();
        nodes.forEach(t -> graph.addVertex(t.getId()));
        nodes.forEach(t-> t.getLinks().forEach(link -> graph.addEdge(t.getId(),link.toString())));

        ArrayList<ArrayList<Integer>> paths = new ArrayList<>();
        paths.add(new ArrayList<>());
        algorithm(graph, from, to, paths);
        return ok(Json.toJson(graph.getAdjVertices()));
    }


}
