package io.training.api.controllers;

import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.*;
import io.training.api.models.Transaction;
import io.training.api.models.responses.ChartData;
import io.training.api.mongo.IMongoDB;
import org.bson.Document;
import org.bson.conversions.Bson;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;

import static com.mongodb.client.model.Aggregates.*;

import javax.inject.Inject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class AssignmentLectureTenController extends Controller {
    @Inject
    IMongoDB mongoDB;

    public Result paginated(Http.Request request, Integer limit, Integer skip, String token) {
        MongoCollection<Transaction> collection = mongoDB.getMongoDatabase().getCollection("transactions", Transaction.class);
        collection.find().limit(limit).skip(skip);
        List<Transaction> transaction = collection.find().limit(limit).skip(skip).into(new ArrayList<>());
        System.out.println(transaction);
        return ok(Json.toJson(transaction));
    }

    public Result subscribe(Http.Request request) {


        return ok();
    }

    public Result assignment1(Http.Request request) {
        MongoCollection<Document> collection = mongoDB.getMongoDatabase().getCollection("transactions", Document.class);

        AggregateIterable<Document> data = collection.aggregate(Arrays.
                asList(group("$categoryName", Accumulators.sum("value", "$salesIncVatActual")),
                        sort(Sorts.ascending("_id")),
                        project(Projections.fields(
                                Projections.computed("name", "$_id"),
                                Projections.exclude("_id"),
                                Projections.include("value")
                        ))));
        ArrayList<String> categoryNames = collection.distinct("categoryName", String.class).into(new ArrayList<>());
        Document chartData = new Document();
        chartData.append("xAxis", new Document()
                .append("type", "category")
                .append("data", categoryNames)
        );
        chartData.append("yAxis", new Document()
                .append("type", "value")
        );
        chartData.append("series", new Document()
                .append("type", "line")
                .append("areaStyle", new Document("color", "#1b9ad1"))
                .append("color", "#293642")
                .append("data", data.into(new ArrayList<>()))
        );
        return ok(Json.toJson(chartData));
    }

    public Result assignment2(Http.Request request) {
        MongoCollection<Document> collection = mongoDB.getMongoDatabase().getCollection("transactions", Document.class);
        List<Bson> pipeline = new ArrayList<>();
        pipeline.add(Aggregates.project(
                Projections.fields(
                        Projections.include("brandName"),
                        Projections.include("categoryName"),
                        Projections.include("salesIncVatActual"),
                        Projections.exclude("_id")
                )
        ));
        pipeline.add(Aggregates.group(new Document("brandName", "$brandName").
                        append("categoryName", "$categoryName"),
                Accumulators.sum("salesIncVatActual", "$salesIncVatActual")
        ));

        pipeline.add(Aggregates.project(
                Projections.fields(
                        Projections.computed("categoryName", "$_id.categoryName"),
                        Projections.computed("brandName", "$_id.brandName"),
                        Projections.exclude("_id"),
                        Projections.include("salesIncVatActual")
                )));
        pipeline.add(Aggregates.sort(Sorts.ascending("categoryName")));

        pipeline.add(Aggregates.project(Projections.fields(
                Projections.computed("name", "$brandName"),
                Projections.computed("data.name", "$categoryName"),
                Projections.computed("data.value", "$salesIncVatActual")

        )));
        pipeline.add(
                Aggregates.group(new Document("name", "$name"),
                        Accumulators.push("data", "$data"))

        );
        pipeline.add(Aggregates.project(Projections.fields(
                Projections.computed("name", "$_id.name"),
                Projections.computed("data", "$data"),
                Projections.excludeId()

        )));

        pipeline.add(new Document("$limit", 5));
        List<Document> aggregated = collection
                .aggregate(pipeline, Document.class)
                .into(new ArrayList<>());
        aggregated.forEach(document -> document.append("type", "bar"));

        ArrayList<String> categoryNames = collection.distinct("categoryName", String.class).into(new ArrayList<>());
        aggregated.forEach(document -> document.getList("data", Document.class).
                forEach(data -> categoryNames.add(data.get("name", String.class))));

        Document chartData = new Document();
        chartData.append("xAxis", new Document()
                .append("type", "category")
                .append("data", categoryNames.stream().distinct().collect(Collectors.toList()))
        );
        chartData.append("yAxis", new Document()
                .append("type", "value")
        );
        chartData.append("series", aggregated);

        return ok(Json.toJson(chartData));
    }

    public Result assignment3(Http.Request request) {
        MongoCollection<Document> collection = mongoDB.getMongoDatabase().getCollection("transactions", Document.class);
        List<Bson> pipeline = new ArrayList<>();
        pipeline.add(Aggregates.project(Projections.include("categoryName", "salesIncVatActual")));

        pipeline.add(Aggregates.group(new Document("categoryName", "$categoryName"),
                Accumulators.avg("salesIncVatActual", "$salesIncVatActual")));

        pipeline.add(Aggregates.project(Projections.fields(
                Projections.computed("name", "$_id.categoryName"),
                Projections.computed("value", "$salesIncVatActual"),
                Projections.excludeId()
        )));
        pipeline.add(Aggregates.sort(Sorts.descending("value")));
        pipeline.add(new Document("$limit", 10));

        List<ChartData> aggregated = collection
                .aggregate(pipeline, ChartData.class)
                .into(new ArrayList<>());

        Document chartData = new Document();
        chartData.append("series", new Document()
                .append("type", "treemap")
                .append("data", aggregated));

        return ok(Json.toJson(chartData));
    }

    public Result assignment4(Http.Request request) {
        MongoCollection<Document> collection = mongoDB.getMongoDatabase().getCollection("transactions", Document.class);
        List<Bson> pipeline = new ArrayList<>();
        pipeline.add(Aggregates.project(Projections.include("categoryName", "salesIncVatActual")));
        pipeline.add(Aggregates.group(new Document("categoryName", "$categoryName"),
                Accumulators.sum("values", "$salesIncVatActual")
                ));

        pipeline.add(Aggregates.project(Projections.fields(
                Projections.computed("name", "$_id.categoryName"),
                Projections.computed("value", "$values"),
                Projections.excludeId()
        )));

        pipeline.add(Aggregates.sort(Sorts.descending("value")));
        pipeline.add(new Document("$limit", 4));

        List<Document> aggregated = collection
                .aggregate(pipeline, Document.class)
                .into(new ArrayList<>());

        aggregated.forEach(document -> document.append("itemStyle", new Document().append("normal", new Document().append("color",
                "rgb(60, 185, 226," + ((Double) document.get("value")) / ((Double) aggregated.get(0).get("value")) + ")"))));

        aggregated.forEach(document -> document.get("itemStyle", Document.class).append("emphasis", new Document().append("color",
                ((Double) (document.get("value")) / ((Double) aggregated.get(0).get("value"))) + 0.2 > 1 ?
                        "rgb(60, 185, 226," + 1 + ")" :
                        "rgb(60, 185, 226," + ((Double) (document.get("value")) / ((Double) aggregated.get(0).get("value"))) + 0.2 + ")"
        )));

        Document document = new Document();
        document.append("series", new Document().append("type", "pie"));
        document.get("series", Document.class).append("data", aggregated);
        return ok(Json.toJson(document));
    }

    public Result assignment5(Http.Request request) {
        MongoCollection<Document> collection = mongoDB.getMongoDatabase().getCollection("transactions", Document.class);

        List<Bson> pipeline = new ArrayList<>();
        pipeline.add(Aggregates.project(Projections.include("brandName", "salesIncVatActual", "volume")));
        Document chartData = new Document();
        Document data = new Document();
        Document visualMap = new Document();
        data.append("data", new ArrayList<String>());

        pipeline.add(Aggregates.group(new Document("name", "$brandName"),
                Accumulators.sum("salesIncVatActual", "$salesIncVatActual"),
                Accumulators.max("volume", "$volume")));
        pipeline.add(Aggregates.project(Projections.fields(
                Projections.computed("name", "$_id.name"),
                Projections.excludeId(),
                Projections.include("volume", "salesIncVatActual")
        )));

        pipeline.add(Aggregates.sort(Sorts.descending("salesIncVatActual")));
        pipeline.add(new Document("$limit", 50));

        List<Document> aggregated = collection
                .aggregate(pipeline, Document.class)
                .into(new ArrayList<>());

        aggregated.forEach(document -> document.append("value",
                ((Double) document.get("salesIncVatActual")) * ((Double) document.get("volume"))));
        aggregated.forEach(document -> data.get("data", List.class).add(document.get("name")));
        aggregated.forEach(document -> document.remove("salesIncVatActual"));
        aggregated.forEach(document -> document.remove("volume"));

        visualMap.append("top", 10).append("right", 10).append("min", 0).append("max", 93431.41).append("type", "continuous");
        chartData.append("xAxis", new Document().append("type", "category"));
        chartData.get("xAxis", Document.class).append("data", data.get("data"));
        chartData.append("yAxis", new Document().append("type", "value"));
        chartData.append("visualMap", visualMap).append("series", new Document().append("type", "bar")
                .append("data", aggregated));

        return ok(Json.toJson(chartData));
    }

    public Result assignment6(Http.Request request) {
        MongoCollection<Document> collection = mongoDB.getMongoDatabase().getCollection("transactions", Document.class);
        List<Bson> pipeline = new ArrayList<>();
        pipeline.add(Aggregates.project(Projections.include("brandName", "salesIncVatActual")));
        pipeline.add(Aggregates.group(new Document("name", "$brandName"),
                Accumulators.max("salesIncVatActual", "$salesIncVatActual")
        ));

        pipeline.add(Aggregates.project(Projections.fields(
                Projections.computed("value", "$salesIncVatActual"),
                Projections.computed("name", "$_id.name"),
                Projections.excludeId()
        )));

        List<Document> aggregated = collection
                .aggregate(pipeline, Document.class)
                .into(new ArrayList<>());

        List<Double> values = new ArrayList<>();
        aggregated.forEach(document -> values.add(document.get("value", Double.class)));
        Double lt10 = values.stream().filter((number) -> number < 10).reduce(0.0, Double::sum);
        Double gt10lt100 = values.stream().filter((number) -> number > 10 && number < 100).reduce(0.0, Double::sum);
        Double gt100 = values.stream().filter((number) -> number > 100).reduce(0.0, Double::sum);
        List<Document> data = new ArrayList<>();
        data.add(new Document("name", "0-10").append("value", lt10));
        data.add(new Document("name", "10-100").append("value", gt10lt100));
        data.add(new Document("name", "100+").append("value", gt100));


        Document chartData = new Document();


        chartData.append("series", new Document()
                .append("type", "treemap")
                .append("data", data));

        return ok(Json.toJson(chartData));
    }


}





