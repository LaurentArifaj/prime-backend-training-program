package io.training.api.controllers;

import akka.Done;
import akka.http.javadsl.server.RoutingJavaMapping;
import akka.stream.Materializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.util.JSONPObject;
import com.google.common.base.Strings;
import com.google.inject.Singleton;
import com.mongodb.BasicDBObject;
import com.mongodb.client.*;
import io.training.api.actions.WithCache;
import io.training.api.models.BaseModel;
import io.training.api.models.Taxi;
import io.training.api.models.validators.HibernateValidator;
import io.training.api.mongo.IMongoDB;
import io.training.api.utils.DatabaseUtils;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.*;
import play.libs.Json;
import io.training.api.services.SerializationService;

import java.io.*;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import com.google.inject.Inject;

import io.training.api.models.User;
import scala.util.parsing.json.JSONObject;

import javax.cache.Cache;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;

@Singleton
public class AssignmentLectureNineController extends Controller {

    @Inject
    IMongoDB mongoDB;


    public Result setup(Http.Request request) {

        List<User> users = new ArrayList<>();
        MongoCollection<User> collection = mongoDB.getMongoDatabase()
                .getCollection("Users", User.class);
        User[] user = Json.fromJson(request.body().asJson(), User[].class);
        users.addAll(Arrays.asList(user));
        users.stream().forEach(e -> {
            if (collection.find(new Document("id", e.getId())).first() == null) {
                collection.insertOne(e);
            }
        });
        return ok(Json.toJson(users));
    }

    public Result all(Http.Request request) {
        MongoCollection<User> collection = mongoDB.getMongoDatabase().getCollection("Users", User.class);
        List<User> users = collection.find().into(new ArrayList<>());
        return ok(Json.toJson(users));
    }

    public Result save(Http.Request request) {
        User user;
        try {
            user = Json.fromJson(request.body().asJson(), User.class);
            String errors = HibernateValidator.validate(user);
            if (!Strings.isNullOrEmpty(errors)) {
                return badRequest(errors);
            }
            MongoCollection<User> collection = mongoDB.getMongoDatabase().getCollection("Users", User.class);
            user.setId(new ObjectId().toString());
            collection.insertOne(user);
            return ok(Json.toJson(user));

        } catch (Exception ex) {
            return badRequest(ex.toString());
        }
    }

    public Result update(Http.Request request, String id) {
        User user;
        MongoCollection<User> collection = mongoDB.getMongoDatabase().getCollection("Users", User.class);
        if (collection.find(new Document("id", id)).first() == null) {
            return notFound();
        }
        try {
            user = Json.fromJson(request.body().asJson(), User.class);
            String errors = HibernateValidator.validate(user);
            if (!Strings.isNullOrEmpty(errors)) {
                return badRequest(errors);
            }
            collection.replaceOne(new Document("id", id), user);
            return ok(Json.toJson(user));
        } catch (Exception ex) {
            return badRequest(ex.toString());
        }
    }

    public Result delete(Http.Request request, String id) {
        MongoCollection<User> collection = mongoDB.getMongoDatabase().getCollection("Users", User.class);
        if (collection.find(new Document("id", id)).first() == null) {
            return notFound();
        }
        User user = Json.fromJson(request.body().asJson(), User.class);
        collection.deleteOne(new Document("id", id));
        return ok(Json.toJson(user));
    }

}
