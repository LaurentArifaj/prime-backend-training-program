package io.training.api.controllers;

import io.training.api.models.BinaryTree;
import io.training.api.models.User;
import io.training.api.models.requests.BinarySearchRequest;
import io.training.api.models.requests.BinaryTreeRequest;
import io.training.api.models.requests.NameValuePair;
import io.training.api.models.responses.ChartData;
import io.training.api.utils.DatabaseUtils;
import lombok.val;
import play.libs.Json;
import play.mvc.*;
import play.Logger;
import io.training.api.services.SerializationService;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.Inject;


import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class AssignmentLectureThreeController extends Controller {
    @Inject
SerializationService serializationService;

    public static List<User> users = new ArrayList<>();

    public Result all(Http.Request request) {
        return ok(Json.toJson(users)); 
   }

    @BodyParser.Of(BodyParser.Json.class)
    public CompletableFuture<Result> save(Http.Request request) {
        User user = Json.fromJson(request.body().asJson(), User.class);
        user.setId(UUID.randomUUID().toString());   
        users.add(user);
        return serializationService.toJsonNode(user).thenApply(Results::ok);
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result update(Http.Request request, String id) {
        User user = Json.fromJson(request.body().asJson(), User.class);
        User.updateUser(users.stream().filter(u -> u.getId().equals(id)).findFirst().get(), user);
        return ok(Json.toJson(user));    
    }

    @BodyParser.Of(BodyParser.Json.class)
    public CompletableFuture<Result> delete(Http.Request request, String id) {
        User user = Json.fromJson(request.body().asJson(), User.class);
        users.remove(users.stream().filter(u -> u.getId().equals(id)).findFirst().get());
        return serializationService.toJsonNode(user).thenApply(Results::ok);
    }    
 
    public Result averages(Http.Request request) {
        List<ChartData> chartData = users.stream().collect(Collectors.
            groupingBy(User::getType, Collectors.averagingInt(User::getAge))).
            entrySet().stream().map(t -> new ChartData(t.getKey(), t.getValue())).
            collect(Collectors.toList());
        return ok(Json.toJson(chartData)); 
    }    

    public Result types(Http.Request request) {
        List<ChartData> chartData = users.stream().collect(Collectors.
            groupingBy(User::getGender, Collectors.counting())).
            entrySet().stream().map(t -> new ChartData(t.getKey(),(double)t.getValue())).
            collect(Collectors.toList());
        return ok(Json.toJson(chartData)); 
    }    

    public Result binaryTree(Http.Request request) {
        BinaryTreeRequest binaryTree =Json.fromJson(request.body().asJson(), BinaryTreeRequest.class);
        int value = binaryTree.getValue();
        BinaryTree tree = binaryTree.getTree();        
        BinaryTree treeToReturn = new BinaryTree(); 

        while(true){
            if (tree.getValue() == value ) {
                treeToReturn = tree;
                break;
            }
            else if (tree.getRight() == null){
                treeToReturn =  new BinaryTree();
                break;
            }
            else if (tree.getValue() > value){
                tree = tree.getLeft();
            }
            else if (tree.getValue() < value) {
                tree = tree.getRight();
            }
         }
        return ok(Json.toJson(treeToReturn));
//      Solution using recursive function findValue
//        return ok(Json.toJson(BinaryTree.findValue(tree, value)));
        }
}