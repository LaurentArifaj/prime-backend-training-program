package io.training.api.controllers;

import akka.Done;
import akka.http.javadsl.server.RoutingJavaMapping;
import akka.stream.Materializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.util.JSONPObject;
import com.google.common.base.Strings;
import com.google.inject.Singleton;
import io.training.api.actions.WithCache;
import io.training.api.models.validators.HibernateValidator;
import play.cache.AsyncCacheApi;
import play.cache.Cached;
import play.cache.SyncCacheApi;
import play.cache.redis.AsyncRedisList;
import play.cache.redis.AsyncRedisSet;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.*;
import play.cache.NamedCache;
import play.libs.Json;
import io.training.api.services.SerializationService;

import java.io.*;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import com.google.inject.Inject;

import io.training.api.models.User;
import scala.util.parsing.json.JSONObject;

import javax.cache.Cache;

@Singleton
public class AssignmentLectureSixController extends Controller {
    SerializationService serializationService;

    @Inject
    HttpExecutionContext ec;

    @Inject
    @NamedCache("training")
    AsyncCacheApi cacheApi;

    List<User> users = new ArrayList<>();

    public Result setup(Http.Request request) throws IOException {
        User[] user = Json.fromJson(request.body().asJson(), User[].class);
        users.addAll(Arrays.asList(user));
        users.forEach(userr -> cacheApi.set(userr.getId(), userr.toString()));
        String json = Json.stringify(Json.toJson(users));
        cacheApi.set("users", json);
        return ok(Json.toJson(users));
    }

    public CompletableFuture<Result> all(Http.Request request){
        return CompletableFuture.supplyAsync(() -> {
            ArrayList<User> usersList = new ArrayList<>();
            try {
                Object check = cacheApi.getOptional("users").toCompletableFuture().get().get();
                User[] usr = Json.fromJson(Json.parse(check.toString()), User[].class);
                usersList.addAll(Arrays.asList(usr));
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
            return ok(Json.toJson(usersList));
        }, ec.current());
     }

    @BodyParser.Of(BodyParser.Json.class)
    public CompletableFuture<Result> save(Http.Request request) {
        return CompletableFuture.supplyAsync(() -> {
        try{
            User user = Json.fromJson(request.body().asJson(), User.class);
            String errors = HibernateValidator.validate(user);
            if (!Strings.isNullOrEmpty(errors)) {
                 return badRequest(errors);
            }
            return ok(Json.toJson(user));
            } catch (Exception ex) {
//                ex.printStackTrace();
                return badRequest("Wrong data");
            }
        }, ec.current());
        }

    @BodyParser.Of(BodyParser.Json.class)
    public CompletableFuture<Result> update(Http.Request request, String id) {
        return CompletableFuture.supplyAsync(() -> {
            try{
                User user = Json.fromJson(request.body().asJson(), User.class);
                Object check = cacheApi.getOptional("users").toCompletableFuture().get().get();
                User[] usr = Json.fromJson(Json.parse(check.toString()), User[].class);
                ArrayList<User> usersList = new ArrayList<>(Arrays.asList(usr));
                String errors = HibernateValidator.validate(user);
                if (!Strings.isNullOrEmpty(errors)) {
                    return badRequest(errors);
                }
                if (usersList.stream().anyMatch(u -> u.getId().equals(id))) {

                    User.updateUser(usersList.stream().filter(u -> u.getId().equals(id)).findFirst().get(), user);
                    cacheApi.remove("users");
                    String json = Json.stringify(Json.toJson(usersList));
                    cacheApi.set("users", json);
                    return ok(Json.toJson(usersList.get(0)));
                }
                else {
                    return notFound();
                }
          } catch (Exception ex) {
//                ex.printStackTrace();
                return badRequest("Wrong data");
            }
        }, ec.current());
    }


    @BodyParser.Of(BodyParser.Json.class)
    public CompletableFuture<Result> delete(Http.Request request, String id){
        return CompletableFuture.supplyAsync(() -> {
            try{
                Object check = cacheApi.getOptional("users").toCompletableFuture().get().get();
                User[] usr = Json.fromJson(Json.parse(check.toString()), User[].class);
                ArrayList<User> usersList = new ArrayList<>(Arrays.asList(usr));
//                User user = Json.fromJson(request.body().asJson(), User.class);
//                    usersList.forEach(h -> System.out.println(h.getId()));
//                    System.out.println(id);
                if (usersList.stream().anyMatch(u -> u.getId().equals(id))){
                    usersList.remove(usersList.stream().filter(u -> u.getId().equals(id)).findFirst().get());
                    cacheApi.remove("users");
                    String json = Json.stringify(Json.toJson(usersList));
                    cacheApi.set("users", json);
                    return ok(Json.toJson(usersList.get(0)));
                }
//                if (cacheApi.getOptional(id).toCompletableFuture().get() == Optional.empty()){
//                    return notFound();
//                }
                else {
//                    System.out.println(request.body().asJson());
                    return notFound();
                }
            }catch (Exception ex){
            return notFound();
            }
    }, ec.current());
    }
}