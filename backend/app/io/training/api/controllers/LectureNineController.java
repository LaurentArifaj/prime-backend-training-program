package io.training.api.controllers;

import com.google.inject.Inject;
import com.mongodb.client.ClientSession;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.TransactionBody;
import com.mongodb.client.model.*;
import io.training.api.models.Transaction;
import io.training.api.models.examples.FreeUser;
import io.training.api.models.examples.PremiumUser;
import io.training.api.models.examples.RegisteredUser;
import io.training.api.models.examples.SubscriberUser;
import io.training.api.models.responses.AggregationExample;
import io.training.api.mongo.IMongoDB;
import io.training.api.services.SerializationService;
import io.training.api.utils.DatabaseUtils;
import org.bson.Document;
import org.bson.conversions.Bson;
import play.Logger;
import play.libs.Json;
import play.mvc.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

public class LectureNineController extends Controller {
	@Inject
	IMongoDB mongoDB;

	@Inject
	SerializationService serializationService;

	public Result all (Http.Request request) {
		MongoCollection<RegisteredUser> collection = mongoDB.getMongoDatabase()
			.getCollection("pojo_user", RegisteredUser.class);

		List<RegisteredUser> items = collection
			.find()
			.into(new ArrayList<>());

		items.forEach(this::log);

		return ok(Json.toJson(items));
	}

	@BodyParser.Of(BodyParser.Json.class)
	public Result save (Http.Request request) {
		Optional<RegisteredUser> optionalItem = request.body().parseJson(RegisteredUser.class);
		if (!optionalItem.isPresent()) {
			return badRequest("no no no");
		}
		RegisteredUser user = optionalItem.get();
		this.log(user);
		MongoCollection<RegisteredUser> collection = mongoDB.getMongoDatabase()
				.getCollection("pojo_user", RegisteredUser.class);

		collection.insertOne(user);
		return ok(Json.toJson(user));
	}

	private void log(RegisteredUser next) {
		if (next instanceof PremiumUser) {
			PremiumUser premiumUser = (PremiumUser) next;
			Logger.of(this.getClass()).debug("Got a premium user: {}", premiumUser.toString());
		} else if (next instanceof FreeUser) {
			FreeUser freeUser = (FreeUser) next;
			Logger.of(this.getClass()).debug("Got a free user: {}", freeUser.toString());
		} else if (next instanceof SubscriberUser) {
			SubscriberUser subscriberUser = (SubscriberUser) next;
			Logger.of(this.getClass()).debug("Got a subscriber user: {}", subscriberUser.toString());
		} else {
			Logger.of(this.getClass()).debug("Got a registered user: {}", next.toString());
		}
	}

	@BodyParser.Of(BodyParser.MultipartFormData.class)
	public CompletableFuture<Result> ingest (Http.Request request) throws IOException {
		return serializationService.parseFileOfType(request, "data", Transaction.class)
			.thenApplyAsync((items) -> {
				mongoDB.getMongoDatabase()
					.getCollection("transactions", Transaction.class).insertMany(items);
				return Json.newObject();
			})
			.thenApply(Results::ok)
			.exceptionally(DatabaseUtils::throwableToResult);
	}

	public Result filters (Http.Request request) {
		MongoCollection<Transaction> collection = mongoDB
				.getMongoDatabase()
				.getCollection("transactions", Transaction.class);

		List<String> uniques = collection
			.distinct("categoryName", String.class)
			.into(new ArrayList<>());

		FindIterable<Transaction> find = collection
			.find()
			.filter(Filters.eq("brandName", "ProdL"))
			.sort(Sorts.orderBy(
				Sorts.ascending("brandName"),
				Sorts.descending("dateTime")
			))
			.skip(50)
			.limit(50);
		return ok(Json.toJson(find.into(new ArrayList<>())));
	}

	public Result aggregations (Http.Request request) {

		MongoCollection<Transaction> collection = mongoDB
				.getMongoDatabase()
				.getCollection("transactions", Transaction.class);

		List<Bson> pipeline = new ArrayList<>();

		pipeline.add(Aggregates.project(
			Projections.fields(
				Projections.include("brandName"),
				Projections.include("categoryName"),
				Projections.include("volume"),
				Projections.include("salesIncVatActual"),
				Projections.exclude("_id")
			)
		));

//		pipeline.add(
//			Aggregates.match(Filters.eq("brandName", "ProdL"))
//		);
		pipeline.add(
			Aggregates.group(new Document("name", "$brandName")
					.append("category", "$categoryName"),
					Accumulators.sum("total", "$volume"),
					Accumulators.sum("sales", "$salesIncVatActual")
			)
		);
		pipeline.add(Aggregates.project(
			Projections.fields(
			Projections.computed("brand", "$_id.name"),
				Projections.computed("category", "$_id.category"),
				Projections.exclude("_id"),
				Projections.include("total", "sales")
			)
		));

		pipeline.add(new Document("$limit", 50));

		Logger.of(this.getClass()).debug("Executing Pipeline with:");
		pipeline.forEach((next) -> Logger.of(this.getClass()).debug("Stage {}", next.toString()));

		List<AggregationExample> aggregated = collection
				.aggregate(pipeline, AggregationExample.class)
				.into(new ArrayList<>());

		return ok(Json.toJson(aggregated));
	}

}