/**
 * Created by LeutrimNeziri on 09/04/2019.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import { PAGES } from 'Constants';
import Intro from "pages/lecture3/Intro";
import PlayIntro from "pages/lecture3/PlayIntro";
import RestAPI from "pages/lecture3/RestAPI";
import React from "react";

const styles = () => ({
  root: {},
})

class Index extends React.Component {
  render() {
    const { breadcrumbs } = this.props

    let section = breadcrumbs[0]
    if (breadcrumbs.length > 1) {
      section = breadcrumbs[1]
    }

    const props = {
      section
    }

    switch (section.id) {
      case PAGES.LECTURE_3.PLAY_INTRODUCTION:
        return <PlayIntro {...props} />
      case PAGES.LECTURE_3.REST_API:
        return <RestAPI {...props} />
    }
    return <Intro {...props} />
  }
}

export default withStyles(styles)(Index)
