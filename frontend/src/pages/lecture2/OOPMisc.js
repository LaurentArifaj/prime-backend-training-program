/**
 * Created by LeutrimNeziri on 09/04/2019.
 */
import Code from "presentations/Code";
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import SimpleLink from "presentations/rows/SimpleLink";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import { Italic, Bold } from "presentations/Label";
import JavaConstructorVsMethodImage from 'assets/images/lecture2/constructor-vs-method-in-java.jpg'
import InheritanceLevelsImage from 'assets/images/lecture2/inheritance-levles.png'
import InheritanceVariantsImage from 'assets/images/lecture2/inheritance-variance.png'
import JavaTypePromotionImage from 'assets/images/lecture2/java-type-promotion.png'
import EncapsulationImage from 'assets/images/lecture2/encapsulation.png'
import AbstractClassImage from 'assets/images/lecture2/abstract-class-in-java.jpg'
import WhyUseInterfaceImage from 'assets/images/lecture2/why-use-java-interface.jpg'

const styles = ({ size, typography }) => ({
  root: {},
  image: {
    marginRight: size.spacing
  },
  horizontal: {
    display: 'flex',
    flexFlow: 'row nowrap',
    alignItems: 'center',
    alignContent: 'flex-start',
    '& $vertical': {
      marginRight: size.spacing
    }
  },
  vertical: {
    display: 'flex',
    flexFlow: 'column wrap',
    alignItems: 'flex-start',
    alignContent: 'flex-start'
  },
})

const instanceOfCodeExample = `class A { public void display(){ System.out.println("Class A"); } }
class B extends A { public void display() { System.out.println("Class B"); } } }

A objA = new B();
if(objA instanceof B){
  ((B)objA).display();
}`

const finalVariableCodeExample = `class Bike {  
 final int speedlimit=90;//final variable  
 void run(){  
  speedlimit=400;  
 }
}
Bike9 obj=new  Bike9();  
obj.run();
// Output: Compile Time Error`

const finalMethodCodeExample = `class Bike{
  final void run(){System.out.println("running");}
}
   
class Honda extends Bike{
   void run(){System.out.println("running safely with 100kmph");}
}

Honda honda= new Honda();
honda.run();
// Output: Compile Time Error`

class OOPMisc extends React.Component {
  render() {
    const { classes, section } = this.props
    let keywords = section.children[0]
    let accessModifiers = section.children[1]
    let objectCasting = section.children[2]
    return (
      <Fragment>
        <Typography variant={'heading'}>
          {section.display}
          <Divider />
        </Typography>
        <Typography id={keywords.id} variant={'title'}>
          {keywords.display}
        </Typography>

        <Typography variant={'section'}>
          Keyword: "this" in Java
        </Typography>
        <Typography>
          There can be a lot of usage of java this keyword. In java, this is a reference variable that refers to the current object.
        </Typography>
        <Typography>
          Here is given the 6 usage of java this keyword.
          <ol>
            <li>this can be used to refer current class instance variable.</li>
            <li>this can be used to invoke current class method (implicitly)</li>
            <li>this() can be used to invoke current class constructor.</li>
            <li>this can be passed as an argument in the method call.</li>
            <li>this can be passed as argument in the constructor call.</li>
            <li>this can be used to return the current class instance from the method.</li>
          </ol>
        </Typography>
        <Typography variant={'section'}>
          Super Keyword in Java
        </Typography>
        <Typography>
          The super keyword in Java is a reference variable which is used to refer immediate parent class object:
          <ol>
            <li>super can be used to refer immediate parent class instance variable.</li>
            <li>super can be used to invoke immediate parent class method.</li>
            <li>super() can be used to invoke immediate parent class constructor.</li>
          </ol>
        </Typography>
        <Typography variant={'section'}>
          Final Keyword In Java
        </Typography>
        <Typography>
          The final keyword in java is used to restrict the user. If you make any variable as final, you cannot change the value of final variable (It will be constant). The java final keyword can be used in many context. Final can be:
          <ol>
            <li>variable</li>
            <li>method</li>
            <li>class</li>
          </ol>
          The final keyword can be applied with the variables, a final variable that have no value it is called blank final variable or uninitialized final variable. It can be initialized in the constructor only. The blank final variable can be static also which will be initialized in the static block only. We will have detailed learning of these. Let's first learn the basics of final keyword.
        </Typography>

        <Typography>
          Example final variable:
          <Code>
            {finalVariableCodeExample}
          </Code>
          Example final method:
          <Code>
            {finalMethodCodeExample}
          </Code>
        </Typography>
        <Typography variant={'section'}>
          Static Keyword In Java
        </Typography>
        <Typography>
          The static keyword in Java is used for memory management mainly. We can apply static keyword with variables, methods, blocks and nested classes. The static keyword belongs to the class than an instance of the class.
        </Typography>
        <Typography>
          The static can be:
          <ol>
            <li>Variable (also known as a class variable)</li>
            <li>Method (also known as a class method)</li>
            <li>Block</li>
            <li>Nested class</li>
          </ol>
        </Typography>
        <Typography id={accessModifiers.id} variant={'title'}>
          {accessModifiers.display}
        </Typography>

        <Typography>
          There are two types of modifiers in Java: access modifiers and non-access modifiers.<br/>
          The access modifiers in Java specifies the accessibility or scope of a field, method, constructor, or class. We can change the access level of fields, constructors, methods, and class by applying the access modifier on it.
        </Typography>

        <Typography>
          There are four types of Java access modifiers:
          <ol>
            <li><Bold>Private</Bold>: The access level of a private modifier is only within the class. It cannot be accessed from outside the class.</li>
            <li><Bold>Default</Bold>:: The access level of a default modifier is only within the package. It cannot be accessed from outside the package. If you do not specify any access level, it will be the default.</li>
            <li><Bold>Protected</Bold>:: The access level of a protected modifier is within the package and outside the package through child class. If you do not make the child class, it cannot be accessed from outside the package.</li>
            <li><Bold>Public</Bold>:: The access level of a public modifier is everywhere. It can be accessed from within the class, outside the class, within the package and outside the package.</li>
          </ol>
        </Typography>

        <Typography fontStyle={'italic'}>
          If you make any class constructor private, you cannot create the instance of that class from outside the class.
        </Typography>

        <Typography id={objectCasting.id} variant={'title'}>
          {objectCasting.display}
        </Typography>
        <Typography>
          Java type casting is classified into two types:
          <ol>
            <li>Widening casting (implicit): automatic type conversion.
              <Code>
                Superclass superRef = new Subclass();
              </Code>
            </li>
            <li>Narrowing casting (explicit): need explicit conversion.
              <Code>
                Subclass ref = (Subclass) superRef;
              </Code>
            </li>
          </ol>
          We have to be careful when narrowing. When narrowing, we convince the compiler to compile without any error. If we convince it wrongly, we will get a run time error (usually ClassCastException).
        </Typography>
        <Typography>
          In order to perform narrowing correctly, we use the instanceof operator. It checks for an IS-A relationship.
          <Code>
            {instanceOfCodeExample}
          </Code>
          We must remember one important thing when creating an object using the new keyword: the reference type should be the same type or a super type of the object type.
        </Typography>
      </Fragment>
    )
  }
}

export default withStyles(styles)(OOPMisc)
