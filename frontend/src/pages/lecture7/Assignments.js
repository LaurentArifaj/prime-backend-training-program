
/**
 * Created by LeutrimNeziri on 09/04/2019.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import faker from 'faker'
import uuid from 'uuid'
import _ from 'underscore'
import withTests from 'middleware/withTests'
import classNames from 'classnames'
import {
  Button,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Snackbar
} from "@material-ui/core";
import Code from "presentations/Code";
import SimpleLink from "presentations/rows/SimpleLink";

const styles = ({ typography, size }) => ({
  setup: {
    position: 'relative',
    width: '100%',
    height: 80
  },
  headRow: {
    height: 'auto',
  },
  head: {
    width: 120,
    padding: 8,
    paddingTop: 16,
    paddingBottom: 16
  },
  cellRow: {
    height: 'auto',
  },
  cell: {
    padding: 8,
    paddingTop: 16,
    paddingBottom: 16
  },
  code: {
    width: 540
  }
})

const rowStyles = ({ typography, size }) => ({
  code: {
    padding: 0,
    maxHeight: 120
  },
  expected: {
   fontSize: size.captionFontSize
  },
  set: {
    color: 'green',
  },
  error: {
    color: 'red'
  }
})

const model = `{
  id: 'my id', 
  type: 'user type',
  gender: 'M or F',
  age: 0 - 90,
  name: 'a name',
  username: 'user name',
  lastName: 'last name',
  avatar: 'a url to an avatar'
}`

const Row = (props) => {
  const { request, classes, enabled = true, onResponseCopied } = props
  const {
    endpoint,
    method,
    body,
    code,
    expected,
    onCompare = (input, output) => {
      return true
    },
    onSuccess = (input, output) => {
      return code === 200
    },
    onError = (input, error) => {
      return error.status === code
    },
    builder = (input) => {
      const result = {
        endpoint: endpoint,
        options: {
          method: method
        }
      }
      if (method === 'GET') {
        return result;
      }
      return {
        ...result,
        options: {
          method: method,
          body: JSON.stringify(input),
        }
      }
    }
  } = request

  const { error, isLoading, response, runTests } = withTests([request.body], builder, onCompare, onSuccess, onError, enabled)
  const status = error ? error.status : !!response ? 200 : 0
  const isSet = status === code

  let display = 'Success'
  if (isLoading) {
    display = 'Loading'
  } else if (status !== code) {
    display = 'Not Correct, Retry?'
  }

  const onCopy = (event) => {
    event.preventDefault()
    event.stopPropagation()
    navigator.clipboard.writeText(JSON.stringify(expected, null, " "))
    onResponseCopied()
  }

  return (
    <TableRow className={classes.row}>
      <TableCell className={classes.cell}>{request.display}</TableCell>
      <TableCell className={classes.cell}>{`/api${endpoint}`}</TableCell>
      <TableCell className={classes.cell}>{method}</TableCell>
      <TableCell className={classes.cell}>
        <Code className={classes.code}>{JSON.stringify(body, null, " ")}</Code>
      </TableCell>
      <TableCell className={classes.cell}>{expected ? <Button className={classes.expected} onClick={onCopy}>Copy Response</Button> : `${code}`}</TableCell>
      <TableCell className={classes.cell}><Button className={isSet ? classes.set : classes.error} onClick={runTests}>{display}</Button></TableCell>
    </TableRow>
  )
}

const StyledRow = withStyles(rowStyles)(Row)

const Assignments = (props) => {
  const { classes, section } = props

  const [message, setMessage] = React.useState(null)

  const randomUser = () => {
    const gender = faker.name.gender()
    return {
      id: uuid.v1(),
      type: faker.name.jobType(),
      name: faker.name.firstName(gender),
      lastName: faker.name.lastName(gender),
      age: faker.random.number({ min: 10, max: 90 }),
      assets: Array(faker.random.number({ min: 0, max: 3 })).fill(null).map(() => {
        return {
          type: faker.random.arrayElement(["Ranch", "Appartment", "Studio", "House", "Company", "Cash"]),
          value: faker.random.number({ min: 15000, max: 18000000 })
        }
      })
    }
  }

  const companies = Array(30).fill(null)
    .map((next) => ({
      id: uuid.v1(),
      name: faker.company.companyName()
    }))
    .map((next, index, array) => {
      const random = faker.random.number({ min: -1, max: index -1 })
      if (random === -1) {
        return next
      }
      return {...next, parentId: array[random].id }
    })

  const hierarchyFunction = (all, parents) => {
    return parents.map((next, index) => {
      return {...next, children: hierarchyFunction(all, all.filter(which => which.parentId === next.id))}
    })
  }
  const hierarchy = hierarchyFunction(companies, companies.filter(next => !next.parentId))
  const graph = {
    nodes: [{
      id: 0,
      links: [1, 2, 3]
    }, {
      id: 1,
      links: [3]
    }, {
      id: 2,
      links: [0, 1]
    }, {
      id: 3,
      links: []
    }],
    from: 2,
    to: 3
  }
  const allPaths = [[2, 1, 3], [2, 0, 3], [2, 0, 1, 3]]

  const cached = Array(3).fill(null).map(() => {
    return faker.commerce.productName()
  })

  const onResponseCopied = () => {
    setMessage('Response copied to clipboard! Use Ctrl + V to paste it to an editor!')
  }

  const [sections] = React.useState([
    {
      title: 'Filter, Map Reduce',
      info: '',
      requests: [
        {
          display: 'Find the top 4 richest users above 40 years old, based on their total assets!',
          method: 'POST',
          endpoint: '/assignments/lecture7/richest',
          code: 200,
          body: Array(20).fill(null).map(() => randomUser()),
          onCompare: (input, output) => {
            const expected = input
              .filter((a) => a.age > 40)
              .map(next => {
                return {
                  ...next,
                  total: next.assets.reduce((sum, which) => sum + which.value, 0)
                }
              })
              .sort((a, b) => b.total - a.total)
              .slice(0, 4)
              .map((next) => _.omit(next, "total"))

            if (expected.length !== output.length) {
              return false
            }
            return expected.every(next => !!output.find(which => which.id === next.id))
          }
        },
        {
          display: 'Sort the users by their first name and last name initials (Agon Lohaj -> AL). If initials repeat, then sort on Age. Both initials and Age are sorted descending',
          method: 'POST',
          endpoint: '/assignments/lecture7/sorted',
          code: 200,
          body: Array(50).fill(null).map(() => randomUser()).map((next) => ({
            name: next.name,
            lastName: next.lastName,
            age: next.age
          })),
          onCompare: (input, output) => {
            const result = [...input]
              .sort((a, b) => {
                const initial = a.name.charAt(0) + a.lastName.charAt(0)
                const other = b.name.charAt(0) + b.lastName.charAt(0)
                if (initial > other) {
                  return -1
                }
                if (initial < other) {
                  return 1
                }
                return b.age - a.age
              })
            return _.isEqual(result, output)
          }
        },
        {
          display: 'For the given rectangles and a point check if the point is contained within one of the rectangles. If so, return the rectangle otherwise return the closest rectangle to the point! A rectangle is said to be the closest if the distance between its center and the point is lowest!',
          method: 'POST',
          endpoint: '/assignments/lecture7/closest',
          code: 200,
          body: {
            point: {
              x: faker.random.number({ min: 0, max: 90 }),
              y: faker.random.number({ min: 0, max: 90 })
            },
            rectangles: Array(5).fill(null).map(next => ({
              id: uuid.v1(),
              x:  faker.random.number({ min: 10, max: 90 }),
              y:  faker.random.number({ min: 10, max: 90 }),
              width:  faker.random.number({ min: 10, max: 90 }),
              height:  faker.random.number({ min: 10, max: 90 })
            }))
          },
          onCompare: (input, output) => {
            const { point, rectangles } = input
            const found = rectangles.find(which => {
              return which.x <= point.x &&
                which.y <= point.y &&
                which.x + which.width >= point.x &&
                which.y + which.height >= point.y
            })
            const closest = found || rectangles.map(next => {
                const centerX = next.x + (next.width / 2)
                const centerY = next.y + (next.height / 2)
                return {
                  ...next,
                  distance: Math.sqrt(Math.pow(centerX - point.x, 2) + Math.pow(centerY - point.y, 2))
                }
              }).sort((a, b) => {
                return a.distance - b.distance
              })[0]
            return closest.id === output.id
          }
        },
        {
          display: 'Return a new list, for which the companies are nested based on their hierarchy! Don\'t return the id if its null!',
          method: 'POST',
          endpoint: '/assignments/lecture7/hierarchy',
          code: 200,
          body: companies,
          expected: hierarchy,
          onCompare: (input, output) => {
            return _.isEqual(hierarchy, output)
          }
        },
      ]
    },
    {
      title: 'Actions Composition, Authorization',
      info: 'Using Play Actions, build one that does this authorization!',
      requests: Array(4).fill(null).map(_ => {
        const type = faker.random.arrayElement(['Basic', 'Advanced', 'Analyst', 'Admin'])
        return {
          display: 'Only allow the following restricted method to be called if at the headers you see a key: "Authorization" and value: "Admin"',
          method: 'GET',
          endpoint: '/assignments/lecture7/restricted',
          code: type === 'Admin' ? 200 : 403,
          builder: (input) => {
            return {
              endpoint: '/assignments/lecture7/restricted',
              options: {
                method: 'GET',
                headers: {
                  'Authorization': type,
                  'Content-Type': 'application/json',
                }
              }
            }
          }
        }
      })
    },
    {
      title: 'Caching and Async Actions',
      requests: cached.map(next => {
        return {
          display: 'Cache the value if it doesn\'t exist within cache for 1 second. If it exists return it. Also make sure that the requests are ran synchronously, since we don\'t want a race condition',
          method: 'POST',
          endpoint: '/assignments/lecture7/cached',
          code: 200,
          body: next,
          onCompare: (input, output) => {
            return output === cached[0]
          }
        }
      })
    },
    {
      title: 'Backtracking Algorithm! Finding all paths that lead from a source node to a destination!',
      requests: [{
        display: 'Using Backtracking technique I will find all the paths from a source to a destination',
        method: 'POST',
        endpoint: '/assignments/lecture7/backtracking',
        code: 200,
        body: {
          nodes: [{
            id: 0,
            links: [1, 2, 3]
          }, {
            id: 1,
            links: [3]
          }, {
            id: 2,
            links: [0, 1]
          }, {
            id: 3,
            links: []
          }],
          from: 2,
          to: 3
        },
        expected: allPaths,
        onCompare: (input, output) => {
          if (allPaths.length !== output.length) {
            return false
          }
          return allPaths.every(next => output.find(which => _.isEqual(next, which)))
        }
      }]
    }
  ])
  return (
    <Fragment>
      <Typography variant={'heading'}>
        Home Assignments
        <Divider />
      </Typography>
      <Typography>
        Lets exercise in more detail a couple of concepts learned from previous sections!
      </Typography>
      {sections.map((next, index) => {
        const { requests = []} = next
        return (
          <React.Fragment key={index}>
            <Typography>
              {next.title}
            </Typography>
            <Table>
              <TableHead>
                <TableRow className={classes.headRow}>
                  <TableCell className={classes.head}>Name</TableCell>
                  <TableCell className={classes.head}>Api</TableCell>
                  <TableCell className={classes.head}>Method</TableCell>
                  <TableCell className={classNames(classes.head, classes.code)}>Input</TableCell>
                  <TableCell className={classes.head}>Expected Result</TableCell>
                  <TableCell className={classes.head}>Retry?</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {requests.map((next, index) => {
                  return (
                    <StyledRow onResponseCopied={onResponseCopied} classes={{cell: classes.cell, row: classes.cellRow}} key={`call-${index}`} request={next} />
                  )
                })}
              </TableBody>
            </Table>
          </React.Fragment>
        )
      })}
      <Typography>
        Backtracking is an algorithmic-technique for solving problems recursively by trying to build a solution incrementally, one piece at a time, removing those solutions that fail to satisfy the constraints of the problem at any point of time (by time, here, is referred to the time elapsed till reaching any level of the search tree).
      </Typography>
      <Typography>
        Here you can find more information regarding backtracking:
        <ol>
          <li><SimpleLink href="https://www.geeksforgeeks.org/backtracking-algorithms/">https://www.geeksforgeeks.org/backtracking-algorithms/</SimpleLink></li>
          <li><SimpleLink href="https://www.geeksforgeeks.org/find-paths-given-source-destination/">https://www.geeksforgeeks.org/find-paths-given-source-destination/</SimpleLink></li>
        </ol>
      </Typography>
      <Snackbar open={!!message} autoHideDuration={3000} message={message} onClose={() => setMessage(null)} />
    </Fragment>
  )
}

export default withStyles(styles)(Assignments)
