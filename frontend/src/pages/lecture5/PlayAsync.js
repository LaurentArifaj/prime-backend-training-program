/**
 * Created by Agon Lohaj on 27/08/2020.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import SimpleLink from "presentations/rows/SimpleLink";
import { Highlighted } from "presentations/Label";
import Code from "presentations/Code";
import PageLink from "presentations/rows/nav/PageLink";

const styles = ({ typography }) => ({
  root: {},
})

const completionStageExample = `CompletionStage<Double> promiseOfPIValue = computePIAsynchronously();
// Runs in same thread
CompletionStage<Result> promiseOfResult = promiseOfPIValue.thenApply(pi -> ok("PI value computed: " + pi));`

const supplyAsync = `// creates new task
CompletionStage<Integer> promiseOfInt =
    CompletableFuture.supplyAsync(() -> intensiveComputation());`


const httpExecutionContext = `import play.libs.concurrent.HttpExecutionContext;
import play.mvc.*;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

public class MyController extends Controller {

  private HttpExecutionContext httpExecutionContext;

  @Inject
  public MyController(HttpExecutionContext ec) {
    this.httpExecutionContext = ec;
  }

  public CompletionStage<Result> index() {
    // Use a different task with explicit EC
    return calculateResponse()
        .thenApplyAsync(answer -> ok("answer was " + answer), httpExecutionContext.current());
  }

  private static CompletionStage<String> calculateResponse() {
    return CompletableFuture.completedFuture("42");
  }
}`

const httpExecutionContextAnother = `import play.libs.concurrent.HttpExecutionContext;
import play.mvc.*;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

public class MyController extends Controller {

  @Inject
  private HttpExecutionContext httpExecutionContext;

  public CompletionStage<Result> index() {
    // Use a different task with explicit EC
    return calculateResponse()
        .thenApplyAsync(answer -> ok("answer was " + answer), httpExecutionContext.current());
  }

  private static CompletionStage<String> calculateResponse() {
    return CompletableFuture.completedFuture("42");
  }
}`

const customHttpExecutorImports = `import play.libs.concurrent.HttpExecution;

import javax.inject.Inject;
import java.util.concurrent.Executor;
import java.util.concurrent.CompletionStage;
import static java.util.concurrent.CompletableFuture.supplyAsync;`

const myExecutionContext = `public class MongoExecutionContext extends CustomExecutionContext {

	@Inject
	public MongoExecutionContext(ActorSystem actorSystem) {
		// uses a custom thread pool defined in application.conf
		super(actorSystem, "mongo-executor");
	}
}`

const usingCustomExecutor = `@Inject
MongoExecutionContext mongoExecutionContext;

public CompletableFuture<Result> mongo() {
  return CompletableFuture.supplyAsync(() -> {
    List<Taxi> items = mongoDb
      .getMongoDatabase()
      .getCollection("taxi", Taxi.class)
      .find()
      .into(new ArrayList<>());
    return ok(Json.toJson(items));
  }, mongoExecutionContext);
}`

const timeoutHandling = `public CompletableFuture<Result> mongo() {
  return CompletableFuture.supplyAsync(() -> {
    List<Taxi> items = mongoDb
      .getMongoDatabase()
      .getCollection("taxi", Taxi.class)
      .find()
      .into(new ArrayList<>());
    return ok(Json.toJson(items));
  }, mongoExecutionContext);
}

public CompletionStage<Result> mongoWithTimeout() {
  CompletableFuture<Result> future = this.mongo();
  return futures.timeout(future, Duration.of(2, SECONDS)); // The max time to wait on response
}

public CompletionStage<Result> mongoDelayed() {
  CompletableFuture<Result> future = this.mongo();
  return futures.delayed(() -> future, Duration.of(3, SECONDS)); // Execute after an initial delay of 3 seconds
}`

class PlayAsync extends React.Component {
  render() {
    const { classes, section } = this.props
    const asyncControllers = section.children[0]
    const noneBlockingActions = section.children[1]
    const creatingCompletionStages = section.children[2]
    const usingHttpExecutionContext = section.children[3]
    const customHttpExecutionContext = section.children[4]
    const handlingTimeOuts = section.children[5]
    return (
      <Fragment>
        <Typography variant={'heading'}>
          Section 5: {section.display}
          <Typography>
            Resources: <ol>
            <li><SimpleLink href="https://www.playframework.com/documentation/2.8.x/JavaAsync">https://www.playframework.com/documentation/2.8.x/JavaAsync</SimpleLink></li>
            <li><SimpleLink href="https://www.playframework.com/documentation/2.8.x/ThreadPools">https://www.playframework.com/documentation/2.8.x/ThreadPools</SimpleLink></li>
            <li><SimpleLink href="https://doc.akka.io/docs/akka/2.6/dispatchers.html?language=java#setting-the-dispatcher-for-an-actor">https://doc.akka.io/docs/akka/2.6/dispatchers.html?language=java#setting-the-dispatcher-for-an-actor</SimpleLink></li>
          </ol>
          </Typography>
          <Divider />
        </Typography>
        <Typography id={asyncControllers.id} variant={'title'}>
          {asyncControllers.display}
        </Typography>
        <Typography>
          Internally, Play Framework is asynchronous from the bottom up. Play handles every request in an asynchronous, non-blocking way.
        </Typography>
        <Typography>
          The default configuration is tuned for asynchronous controllers. In other words, the application code should avoid blocking in controllers, i.e., having the controller code wait for an operation. Common examples of such blocking operations are JDBC calls, streaming API, HTTP requests and long computations.
        </Typography>
        <Typography>
          Although it’s possible to increase the number of threads in the default execution context to allow more concurrent requests to be processed by blocking controllers, following the recommended approach of keeping the controllers asynchronous makes it easier to scale and to keep the system responsive under load.
        </Typography>

        <Typography id={noneBlockingActions.id} variant={'title'}>
          {noneBlockingActions.display}
        </Typography>
        <Typography>
          Because of the way Play works, action code must be as fast as possible, i.e., non-blocking. So what should we return from our action if we are not yet able to compute the result? We should return the promise of a result!
        </Typography>
        <Typography>
          Java 8 provides a generic promise API called CompletionStage. A <Highlighted>{`CompletionStage<Result>`}</Highlighted> will eventually be redeemed with a value of type Result. By using a <Highlighted>{`CompletionStage<Result>`}</Highlighted> instead of a normal <Highlighted>Result</Highlighted>, we are able to return from our action quickly without blocking anything. Play will then serve the result as soon as the promise is redeemed.
        </Typography>
        <Typography id={creatingCompletionStages.id} variant={'title'}>
          {creatingCompletionStages.display}
        </Typography>
        <Typography>
          To create a <Highlighted>{`CompletionStage<Result>`}</Highlighted> we need another promise first: the promise that will give us the actual value we need to compute the result:
          <Code>
            {completionStageExample}
          </Code>
          Play asynchronous API methods give you a <Highlighted>CompletionStage</Highlighted>. This is the case when you are calling an external web service using the <Highlighted>play.libs.WS</Highlighted> API, or if you are using Akka to schedule asynchronous tasks or to communicate with Actors using <Highlighted>play.libs.Akka</Highlighted>.
        </Typography>
        <Typography>
          In this case, using <Highlighted>CompletionStage.thenApply</Highlighted> will execute the completion stage in the same calling thread as the previous task. This is fine when you have a small amount of CPU bound logic with no blocking.
        </Typography>
        <Typography>
          A simple way to execute a block of code asynchronously and to get a CompletionStage is to use the <Highlighted>CompletableFuture.supplyAsync()</Highlighted> method:
          <Code>
            {supplyAsync}
          </Code>
          Using <Highlighted>supplyAsync</Highlighted> creates a new task which will be placed on the fork join pool, and may be called from a different thread – although, here it’s using the default executor, and in practice you will specify an executor explicitly.
        </Typography>
        <Typography id={usingHttpExecutionContext.id} variant={'title'}>
          {usingHttpExecutionContext.display}
        </Typography>
        <Typography>
          You must supply the HTTP execution context explicitly as an executor when using a Java CompletionStage inside an <PageLink inline to={"/section/action_controllers_responses"}>Action</PageLink>, to ensure that the classloader remains in scope.
        </Typography>
        <Typography>
          You can supply the <Highlighted>play.libs.concurrent.HttpExecutionContext</Highlighted> instance through dependency injection:
          <Code>
            {httpExecutionContext}
          </Code>
          Or like this:
          <Code>
            {httpExecutionContextAnother}
          </Code>
        </Typography>
        <Typography id={customHttpExecutionContext.id} variant={'title'}>
          {customHttpExecutionContext.display}
        </Typography>
        <Typography>
          Using a <Highlighted>CompletionStage</Highlighted> or an <Highlighted>HttpExecutionContext</Highlighted> is only half of the picture though! At this point you are still on Play’s default ExecutionContext. If you are calling out to a blocking API such as JDBC, then you still will need to have your ExecutionStage run with a different executor, to move it off Play’s rendering thread pool. You can do this by creating a subclass of <Highlighted>play.libs.concurrent.CustomExecutionContext</Highlighted> with a reference to the custom dispatcher.
        </Typography>
        <Typography>
          Define a custom execution context:
          <Code>
            {myExecutionContext}
          </Code>
          You will need to define a custom <Highlighted>dispatcher</Highlighted> in application.conf, which is done through <SimpleLink href="https://doc.akka.io/docs/akka/2.6/dispatchers.html?language=java#setting-the-dispatcher-for-an-actor">Akka dispatcher configuration</SimpleLink>.
        </Typography>
        <Typography>
          And now you are ready to use it, like so:
          <Code>
            {usingCustomExecutor}
          </Code>
          <Highlighted>
          You can’t magically turn synchronous IO into asynchronous by wrapping it in a CompletionStage. If you can’t change the application’s architecture to avoid blocking operations, at some point that operation will have to be executed, and that thread is going to block. So in addition to enclosing the operation in a CompletionStage, it’s necessary to configure it to run in a separate execution context that has been configured with enough threads to deal with the expected concurrency. See <SimpleLink href="https://www.playframework.com/documentation/2.8.x/ThreadPools">Understanding Play thread pools</SimpleLink> for more information.
          </Highlighted>
        </Typography>

        <Typography id={handlingTimeOuts.id} variant={'title'}>
          {handlingTimeOuts.display}
        </Typography>
        <Typography>
          It is often useful to handle time-outs properly, to avoid having the web browser block and wait if something goes wrong. You can use the <Highlighted>play.libs.concurrent.Futures.timeout</Highlighted> method to wrap a <Highlighted>CompletionStage</Highlighted> in a non-blocking timeout:
          <Code>
            {timeoutHandling}
          </Code>
        </Typography>
      </Fragment>
    )
  }
}

export default withStyles(styles)(PlayAsync)
