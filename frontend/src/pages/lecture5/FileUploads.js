/**
 * Created by Agon Lohaj on 27/08/2020.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import SimpleLink from "presentations/rows/SimpleLink";
import { Bold, Highlighted } from "presentations/Label";
import Code from "presentations/Code";
import Button from '@material-ui/core/Button'
import { useDispatch } from 'react-redux'
import { CALL_API } from "middleware/Api";

const styles = ({ size, typography }) => ({
  root: {},
  section: {
    display: 'flex',
    flexFlow: 'row wrap',
    justifyContent: 'flex-start',
    width: '100%'
  }
})

const handlingUpload = `import play.libs.Files.TemporaryFile;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;

import java.nio.file.Paths;

public class HomeController extends Controller {

  @BodyParser.Of(BodyParser.MultipartFormData.class)
	public Result upload (Http.Request request) {
		Http.MultipartFormData<Files.TemporaryFile> body = request.body().asMultipartFormData();
		List<Http.MultipartFormData.FilePart<Files.TemporaryFile>> files = body.getFiles();
		if (files.size() == 0) {
			return badRequest().flashing("error", "Missing file");
		}
		Http.MultipartFormData.FilePart<Files.TemporaryFile> file = files.get(0);
		String fileName = file.getFilename();
		return ok(Json.toJson(fileName));
	}
}`

const directFileUploadExample = `public Result upload(Http.Request request) {
  File file = request.body().asRaw().asFile();
  return ok("File uploaded");
}`

const postRequest = `POST  /          controllers.HomeController.upload(request: Request)`

const fileReaper = `play.temporaryFile {
  reaper {
    enabled = true
    initialDelay = "5 minutes"
    interval = "30 seconds"
    olderThan = "30 minutes"
  }
}`

const tree = {
  value: 27,
  left: {
    value: 14,
    left: {
      value: 10
    },
    right: {
      value: 19
    }
  },
  right: {
    value: 35,
    left: {
      value: 31
    },
    right: {
      value: 42
    }
  }
}

const submitJsonExample = JSON.stringify(tree, null, ' ')


const FileUploads = (props) => {
  const { classes, section } = props
  const uploadingMultiformPartData = section.children[0]
  const directFileUpload = section.children[1]
  const cleanUp = section.children[2]
  const input = React.useRef()
  const dispatch = useDispatch()

  const onFileSubmit = (file, url) => {
    const data = new FormData()
    data.append('file', file)
    data.append('description', file.description)
    data.append('name', file.name)
    data.append('filetype', file.name)

    dispatch({
      [CALL_API]: {
        endpoint: url,
        options: {
          method: 'POST',
          body: data,
          headers: {
          }
        }
      }
    })
  }
  /**
   * when the selected file changed
   * @param {Object} e - event raised
   */
  const onFileSelected = (e) => {
    e.preventDefault()
    let files
    if (e.dataTransfer) {
      files = e.dataTransfer.files
    } else if (e.target) {
      files = e.target.files
    }

    if (files.length === 0) {
      return
    }

    onFileSubmit(files[0], `/lecture5/upload`)
  }

  const onChooseFileClicked = (event) => {
    event.preventDefault()
    if (!input.current) {
      return
    }
    input.current.click()
  }

  const onSubmitJSONClicked = (event) => {
    onFileSubmit(new Blob([JSON.stringify(tree, null, 2)], {type: 'application/json'}), `/lecture5/upload/json`)
  }

  return (
    <Fragment>
      <Typography variant={'heading'}>
        Section 5: {section.display}
        <Typography>
          Resources: <SimpleLink href="https://www.playframework.com/documentation/2.8.x/JavaFileUpload">https://www.playframework.com/documentation/2.8.x/JavaFileUpload</SimpleLink>
        </Typography>
        <Divider />
      </Typography>
      <Typography id={uploadingMultiformPartData.id} variant={'title'}>
        {uploadingMultiformPartData.display}
      </Typography>

      <Typography>
        The standard way to upload files in a web application is to use a form with a special <Highlighted>multipart/form-data</Highlighted> encoding, which lets you mix standard form data with file attachment data.
      </Typography>

      <Typography>
        Now define the upload action:
        <Code>
          {handlingUpload}
        </Code>
        The <Highlighted>getRef()</Highlighted> method gives you a reference to a <Highlighted>TemporaryFile</Highlighted>. This is the default way Play handles file uploads.
      </Typography>
      <Typography>
        And finally, add a <Highlighted>POST</Highlighted> route:
        <Code>
          {postRequest}
        </Code>
        <Highlighted>
          <Bold>Note:</Bold> An empty file will be treated just like no file was uploaded at all. The same applies if the filename header of a multipart/form-data file upload part is empty - even when the file itself would not empty.
        </Highlighted>
      </Typography>
      <Typography id={directFileUpload.id} variant={'title'}>
        {directFileUpload.display}
      </Typography>
      <Typography>
        Another way to send files to the server is to use Ajax to upload files asynchronously from a form. In this case, the request body will not be encoded as <Highlighted>multipart/form-data</Highlighted>, but will just contain the plain file contents.
        <Code>
          {directFileUploadExample}
        </Code>
      </Typography>
      <Typography id={cleanUp.id} variant={'title'}>
        {cleanUp.display}
      </Typography>
      <Typography>
        Uploading files uses a <Highlighted>TemporaryFile</Highlighted> API which relies on storing files in a temporary filesystem, accessible through the <Highlighted>getRef()</Highlighted> method. All TemporaryFile references come from a <Highlighted>TemporaryFileCreator</Highlighted> trait, and the implementation can be swapped out as necessary, and there’s now an atomicMoveWithFallback method that uses StandardCopyOption.ATOMIC_MOVE if available.
      </Typography>
      <Typography>
        Uploading files is an inherently dangerous operation, because unbounded file upload can cause the filesystem to fill up – as such, the idea behind <Highlighted>TemporaryFile</Highlighted> is that it’s only in scope at completion and should be moved out of the temporary file system as soon as possible. Any temporary files that are not moved are deleted.
      </Typography>
      <Typography>
        However, under certain conditions, garbage collection does not occur in a timely fashion. As such, there’s also a <Highlighted>play.api.libs.Files.TemporaryFileReaper</Highlighted> that can be enabled to delete temporary files on a scheduled basis using the Akka scheduler, distinct from the garbage collection method.
      </Typography>

      <Typography>
        The reaper is disabled by default, and is enabled through configuration of application.conf:
        <Code>
          {fileReaper}
        </Code>
        The above configuration will delete files that are more than 30 minutes old, using the “olderThan” property. It will start the reaper five minutes after the application starts, and will check the filesystem every 30 seconds thereafter. The reaper is not aware of any existing file uploads, so protracted file uploads may run into the reaper if the system is not carefully configured.
      </Typography>
      <Typography variant="section">
        Exercises
      </Typography>
      <div className={classes.section}>
        <input type="file" ref={input} onChange={onFileSelected}
               style={{width: '100%', display: 'none'}}/>
        <Typography>
          Choose a file and submit it to: /api/lecture5/upload
        </Typography>
        <Button onClick={onChooseFileClicked}>
          Choose File
        </Button>
      </div>
      <div className={classes.section}>
        <input type="file" ref={input} onChange={onFileSelected}
               style={{width: '100%', display: 'none'}}/>
        <Typography>
          Submiting and handling json input to file:
          <Code>
            {submitJsonExample}
          </Code>
        </Typography>
        <Button onClick={onSubmitJSONClicked}>
          Submit JSON as file
        </Button>
      </div>
    </Fragment>
  )
}

export default withStyles(styles)(FileUploads)
