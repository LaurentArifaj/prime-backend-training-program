/**
 * Created by LeutrimNeziri on 30/03/2019.
 */
module.exports = {
  API_URL: 'http://localhost:9000/api',
  PAGES: {
    HOME: 'home',
    LECTURE_1: {
      ID: 'lecture1',
      GETTING_STARTED: 'gettingStarted',
      PROJECT_SETTUP: 'projectSetup',
      AGILE_METHODOLOGY: 'agile',
      WAY_OF_WORKING: 'wayofWorking',
      WORKING_WITH_GIT: 'git'
    },
    LECTURE_2: {
      ID: 'lecture2',
      JAVA_INTRODUCTION: 'JavaIntroduction',
      OOP: 'OOP Principles',
      OOP_MISC: 'OOP Principles Misc',
      JAVA_RELEASES: 'Java New Features',
      ASSIGNMENTS: 'assignments_two'
    },
    LECTURE_3: {
      ID: 'lecture3',
      PLAY_INTRODUCTION: 'PlayIntroduction',
      REST_API: 'Rest API'
    },
    LECTURE_4: {
      ID: 'lecture4',
      PLAY_ROUTING: 'Routing',
      MANIPULATING_RESPONSES: 'Manipulating Responses',
      BODY_PARSERS: 'Body Parsers',
      HANDLING_SERVING_JSON: 'JSON Handling',
      ASSIGNMENTS: 'assignments_four'
    },
    LECTURE_5: {
      ID: 'lecture5',
      PLAY_ASYNC: 'Handling asynchronous results',
      ACTION_COMPOSITION: 'Action Composition',
      FILE_UPLOADS: 'Handling file uploads',
    },
    LECTURE_6: {
      ID: 'lecture6',
      VALIDATION: 'Data Validation',
      DEPENDENCY_INJECTION: 'Dependency Injection',
      REDIS_CACHING: 'Redis Caching',
      ASSIGNMENTS: 'assignments_six',
    },
    LECTURE_7: {
      ID: 'lecture7',
      INTRODUCTION: 'Akka Introduction',
      AKKA_PLAY: 'Akka and Play',
      ASSIGNMENTS: 'assignments_seven'
    },
    LECTURE_8: {
      ID: 'lecture8',
      AKKA_CLUSTER: 'Akka Cluster',
      AKKA_PUB_SUB: 'Akka Pub - Sub'
    },
    LECTURE_9: {
      ID: 'lecture9',
      MONGO_DB: 'Mongo DB',
      MONGO_SIMPLE_QUERIES: 'Simple Queries',
      AGGREGATIONS: 'Aggregations',
      ASSIGNMENTS: 'assignments_nine'
    },
    LECTURE_10: {
      ID: 'lecture10',
      TRANSACTIONS: 'Transactions',
      CHANGE_STREAMS: 'Change Streams',
      INDEXING: 'Indexing',
      REPLICATION: 'Replication',
      ASSIGNMENTS: 'assignments_ten',
    },
    LECTURE_11: {
      ID: 'lecture11',
      DOCUMENTATION: 'Documentation',
      TESTING: 'Testing',
      CD_CI: 'CD_CI',
      ASSIGNMENTS: 'assignments_eleven',
    },
    PLAYGROUND: 'playground',
    SUPPORT: {
      ID: 'support',
      GLOSSARY: 'glossary',
      RESOURCES: 'resources',
      PLAN_PROGRAM: 'program',
      TIPS_AND_TRICKS: 'tips_and_tricks'
    }
  },
}


